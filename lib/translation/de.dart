class TranslationGerman {
  static const String NO_ACCESS_TITLE = "Berechtigung fehlt";
  static const String NO_ACCESS_MESSAGE =
      "Sie haben dieser App die Zugriffsrechte verwehrt. Sie können dies in den Einstellungen ändern.";
  static const String OPEN_APP_SETTINGS = "Einstellungen öffnen";
  static const String FILE = "datei";
  static const String CANCEL = "Abbrechen";
  static const String SEND = "Absenden";
  static const String CONFIRM_SEND = "Absenden";
  static const String TAKE_PICTURE = "Foto aufnehmen";
  static const String UPLOAD_PICTURE = "Foto hochladen";
  static const String UPLOAD_PICTURE_FROM_FILES =
      "Foto aus Dateiordner hochladen";
  static const String UPLOAD_FILE = "Foto hochladen";
  static const String UPLOADING = "wird hochgeladen...";
  static const String UPLOAD_DOCUMENT = "PDF Dokument hochladen";
}
