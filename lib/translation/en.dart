class TranslationEnglish {
  static const String NO_ACCESS_TITLE = "Access not allowed";
  static const String NO_ACCESS_MESSAGE =
      "You have denied access rights to this app. You can change this in the settings.";
  static const String OPEN_APP_SETTINGS = "Open settings";
  static const String FILE = "file";
  static const String CANCEL = "Cancel";
  static const String SEND = "Send";
  static const String CONFIRM_SEND = "Send";
  static const String TAKE_PICTURE = "Take a picture";
  static const String UPLOAD_PICTURE = "Upload a picture";
  static const String UPLOAD_PICTURE_FROM_FILES =
      "Upload image from file folder";
  static const String UPLOAD_FILE = "Upload a file";
  static const String UPLOADING = "uploading...";
  static const String UPLOAD_DOCUMENT = "Upload a PDF Document";
}
