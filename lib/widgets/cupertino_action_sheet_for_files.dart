import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_handle_attachment/models/attached_files_model.dart';
import 'package:flutter_handle_attachment/translation/de.dart';
import 'package:flutter_handle_attachment/translation/en.dart';
import 'package:flutter_handle_attachment/util/converting_files.dart';
import 'package:flutter_handle_attachment/util/create_message_json_string.dart';
import 'package:flutter_handle_attachment/util/file_send_pop_up.dart';
import 'package:flutter_handle_attachment/util/get_inserted_files.dart';
import 'package:flutter_handle_attachment/util/permission_checker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class CupertinoActionSheetActionRow extends StatefulWidget {
  final String title;
  final IconData iconData;
  final bool isTakePicture;
  final bool isUploadPicture;
  final bool isUploadFile;
  final BuildContext context;
  final String? subject;
  final dynamic topicId;
  final String? newMessageTitleControllerText;
  final dynamic fileSize;
  final bool sendDirectly;
  final String sharedPrefAttachmentName;
  final bool allowOnlyPictures;
  final String attachmentFileName;
  final dynamic sharedPreferences;
  final bool isEnglish;
  final Function(String) onSendDirectlyNewMessage;
  final Function(String) onSendDirectlyAddMessage;
  const CupertinoActionSheetActionRow({
    Key? key,
    required this.title,
    this.sendDirectly = true,
    required this.sharedPrefAttachmentName,
    required this.iconData,
    required this.onSendDirectlyNewMessage,
    required this.onSendDirectlyAddMessage,
    this.isEnglish = false,
    this.isTakePicture = false,
    this.isUploadPicture = false,
    this.isUploadFile = false,
    this.newMessageTitleControllerText,
    required this.context,
    required this.sharedPreferences,
    this.subject,
    this.topicId,
    this.fileSize,
    this.allowOnlyPictures = false,
    this.attachmentFileName = '',
  }) : super(key: key);

  @override
  _CupertinoActionSheetActionRowState createState() =>
      _CupertinoActionSheetActionRowState();
}

class _CupertinoActionSheetActionRowState
    extends State<CupertinoActionSheetActionRow> {
  AttachedFilesModel? alreadyAttachedFiles;

  @override
  Widget build(BuildContext context) {
    String fileName =
        widget.isEnglish ? TranslationEnglish.FILE : TranslationGerman.FILE;
    return CupertinoActionSheetAction(
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 4.0, right: 12.0),
              child: Icon(widget.iconData),
            ),
            Text(
              widget.title,
              style: const TextStyle(fontSize: 20.0),
            ),
          ],
        ),
        onPressed: () async {
          if (widget.isTakePicture) {
            if (await checkAndRequestPermission(
                context: context,
                permission: Permission.camera,
                isEnglish: widget.isEnglish)) {
              final ImagePicker _picker = ImagePicker();
              final XFile? photo = await _picker.pickImage(
                source: ImageSource.camera,
                maxHeight: 640,
                maxWidth: 640,
              );

              if (photo != null) {
                // if sendDirectly parameter is set to true
                if (widget.sendDirectly) {
                  String? fileBase64 = createBase64StringFromFile(photo);
                  if (fileBase64 != null) {
                    await userConfirmationSendChatMessageDialog(
                        isEnlish: widget.isEnglish,
                        onSendDirectlyNewMessage:
                            widget.onSendDirectlyNewMessage,
                        onSendDirectlyAddMessage:
                            widget.onSendDirectlyAddMessage,
                        fileInBase64: fileBase64,
                        fileName: widget.attachmentFileName +
                            '_' +
                            fileName +
                            '_' +
                            'upload' +
                            getFileEnding(fileName: photo.name),
                        context: context,
                        subject: widget.subject,
                        topicId: widget.topicId,
                        fileSize: widget.fileSize,
                        newMessageTitleControllerText:
                            widget.newMessageTitleControllerText);
                  }
                } else {
                  bool isAttachmentAvailable = widget.sharedPreferences
                              .getString(widget.sharedPrefAttachmentName) !=
                          null
                      ? true
                      : false;

                  if (isAttachmentAvailable) {
                    alreadyAttachedFiles = getInsertedFilesJson(
                      sharedPreferences: widget.sharedPreferences,
                      sharedPrefAttachmentName: widget.sharedPrefAttachmentName,
                    );
                  }

                  String insertedFiles = createAttachedPicturesJsonString(
                    files: [photo],
                    alreadyInsertedFiles: alreadyAttachedFiles,
                    context: context,
                    fileName: fileName,
                  );

                  // attach the pictures as string to shared Preferences:
                  widget.sharedPreferences.setString(
                      widget.sharedPrefAttachmentName, insertedFiles);
                }
              }
              Navigator.of(context).pop();

              // uploading pictures (png, jpg, jpeg)
              // from pictures folder
            }
          } else if (widget.isUploadPicture) {
            if (await checkAndRequestPermission(
              context: context,
              isEnglish: widget.isEnglish,
              permission: Platform.isIOS
                  ? Permission.photos
                  : Permission.accessMediaLocation,
            )) {
              List<XFile>? files;
              XFile? file;
              final ImagePicker _picker = ImagePicker();
              if (widget.sendDirectly) {
                file = await _picker.pickImage(
                  source: ImageSource.gallery,
                  maxHeight: 640,
                  maxWidth: 640,
                );
              } else {
                files = await _picker.pickMultiImage(
                  maxHeight: 640,
                  maxWidth: 640,
                );
              }

              // if files not null: files have been attached by the user
              if (files != null || file != null) {
                // if sendDirectly parameter is set to true
                if (widget.sendDirectly) {
                  // send user confirmation dialog
                  String? fileBase64 = createBase64StringFromFile(file);
                  if (fileBase64 != null) {
                    await userConfirmationSendChatMessageDialog(
                      isEnlish: widget.isEnglish,
                      onSendDirectlyNewMessage: widget.onSendDirectlyNewMessage,
                      onSendDirectlyAddMessage: widget.onSendDirectlyAddMessage,
                      fileInBase64: fileBase64,
                      fileName: widget.attachmentFileName +
                          '_' +
                          fileName +
                          '_' +
                          'upload' +
                          getFileEnding(fileName: file!.name),
                      context: context,
                      subject: widget.subject,
                      topicId: widget.topicId,
                      fileSize: widget.fileSize,
                      newMessageTitleControllerText:
                          widget.newMessageTitleControllerText,
                    );
                  }

                  // if user wants to create message first:
                } else {
                  if (files != null) {
                    bool isAttachmentAvailable = widget.sharedPreferences
                                .getString(widget.sharedPrefAttachmentName) !=
                            null
                        ? true
                        : false;

                    if (isAttachmentAvailable) {
                      alreadyAttachedFiles = getInsertedFilesJson(
                        sharedPreferences: widget.sharedPreferences,
                        sharedPrefAttachmentName:
                            widget.sharedPrefAttachmentName,
                      );
                    }

                    String insertedFiles = createAttachedPicturesJsonString(
                      files: files,
                      alreadyInsertedFiles: alreadyAttachedFiles,
                      context: context,
                      fileName: fileName,
                    );

                    // attach the pictures as string to shared Preferences:
                    widget.sharedPreferences.setString(
                        widget.sharedPrefAttachmentName, insertedFiles);
                  }
                }
              }
              Navigator.of(context).pop();
            }

            // uploading files (png, jpg, jpeg, pdf)
            // from file folder (important because apple ios has two different locations to access pictures

          } else if (widget.isUploadFile) {
            if (await checkAndRequestPermission(
              context: context,
              isEnglish: widget.isEnglish,
              permission: Platform.isIOS
                  ? Permission.mediaLibrary
                  : Permission.accessMediaLocation,
            )) {
              FilePickerResult? filePickerResult;
              // file picker: accept only specified files
              filePickerResult = await FilePicker.platform.pickFiles(
                allowMultiple: !widget.sendDirectly,
                type: FileType.custom,
                allowedExtensions: widget.allowOnlyPictures
                    ? ['png', 'jpeg', 'jpg']

                    // on ios we must allow all types of files because image picker cannot access the images in file folder
                    // on android, however, this is possible -> image_picker can downscale pictures, file_picker not
                    // -> hence, we only allow pdf on non-ios devices
                    : Platform.isIOS
                        ? ['pdf', 'png', 'jpeg', 'jpg']
                        : ['pdf'],
              );

              // if file(s) atatched:
              if (filePickerResult != null) {
                // if send directly parameter was passed:
                if (widget.sendDirectly) {
                  String? fileBase64 =
                      createBase64StringFromFile(filePickerResult.files.first);
                  if (fileBase64 != null) {
                    await userConfirmationSendChatMessageDialog(
                      isEnlish: widget.isEnglish,
                      onSendDirectlyNewMessage: widget.onSendDirectlyNewMessage,
                      onSendDirectlyAddMessage: widget.onSendDirectlyAddMessage,
                      fileInBase64: fileBase64,
                      fileName: filePickerResult.files.first.name.toString(),
                      fileSize: filePickerResult.files.first.size,
                      context: context,
                      subject: widget.subject,
                      topicId: widget.topicId,
                      newMessageTitleControllerText:
                          widget.newMessageTitleControllerText,
                    );
                  }

                  // if user does not want to send files directly:
                } else {
                  bool isAttachmentAvailable = widget.sharedPreferences
                              .getString(widget.sharedPrefAttachmentName) !=
                          null
                      ? true
                      : false;

                  if (isAttachmentAvailable) {
                    alreadyAttachedFiles = getInsertedFilesJson(
                      sharedPreferences: widget.sharedPreferences,
                      sharedPrefAttachmentName: widget.sharedPrefAttachmentName,
                    );
                  }

                  String insertedFiles = createAttachedFilesJsonString(
                      files: filePickerResult.files,
                      alreadyInsertedFiles: alreadyAttachedFiles,
                      context: context,
                      fileName: fileName);

                  // attach the files as string to shared Preferences:
                  widget.sharedPreferences.setString(
                      widget.sharedPrefAttachmentName, insertedFiles);
                }

                Navigator.of(context).pop();
              }
            }
          }
        });
  }

  // method for image_picker package
  String createAttachedPicturesJsonString({
    required List<XFile> files,
    required AttachedFilesModel? alreadyInsertedFiles,
    required BuildContext context,
    required String fileName,
  }) {
    List alreadyInsertedFilesArray = [];
    if (alreadyInsertedFiles != null) {
      alreadyInsertedFilesArray = alreadyInsertedFiles.files.isNotEmpty
          ? alreadyInsertedFiles.files
          : [];
    }

    var insertedFiles = {};
    dynamic fileMap;
    List filesArray = [];
    for (var file in files) {
      fileMap = {};
      fileMap['base64String'] = convertImage2JPEG(path: file.path);
      fileMap['fileName'] = widget.attachmentFileName +
          '_' +
          fileName +
          '_' +
          'upload' +
          '_${files.indexOf(file)}' +
          getFileEnding(fileName: file.name);
      fileMap['type'] = getFileType(fileName: file.name);

      filesArray.add(fileMap);
    }

    insertedFiles['files'] = filesArray + alreadyInsertedFilesArray;

    String jsonString = json.encode(insertedFiles);
    return jsonString;
  }

  // method for file_picker package
  String createAttachedFilesJsonString({
    required List<PlatformFile> files,
    AttachedFilesModel? alreadyInsertedFiles,
    required BuildContext context,
    required String fileName,
  }) {
    List alreadyInsertedFilesArray = [];
    if (alreadyInsertedFiles != null) {
      alreadyInsertedFilesArray = alreadyInsertedFiles.files.isNotEmpty
          ? alreadyInsertedFiles.files
          : [];
    }
    var insertedFiles = {};
    dynamic fileMap;
    List filesArray = [];
    String fileInBase64 = '';
    for (var file in files) {
      if (file.path != null) {
        fileMap = {};

        String? fileBase64 = createBase64StringFromFile(file);
        if (fileBase64 != null) {
          fileInBase64 = fileBase64;
        }
      }

      fileMap['base64String'] = fileInBase64;
      fileMap['fileName'] = widget.attachmentFileName +
          '_' +
          fileName +
          '_' +
          'upload' +
          '_${files.indexOf(file)}' +
          getFileEnding(fileName: file.name);
      fileMap['type'] = getFileType(fileName: file.name);

      filesArray.add(fileMap);
    }

    insertedFiles['files'] = filesArray + alreadyInsertedFilesArray;

    String jsonString = json.encode(insertedFiles);
    return jsonString;
  }
}
