library flutter_handle_attachment;

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_handle_attachment/translation/de.dart';
import 'package:flutter_handle_attachment/translation/en.dart';

import 'util/fontawesome_pro_icon_data.dart';
import 'widgets/cupertino_action_sheet_for_files.dart';

Future<void> handleAttachments({
  bool allowOnlyPictures = false,
  required BuildContext context,
  String? subject,
  dynamic topicId,
  String? newMessageTitleControllerText,
  String attachmentFileName = '',
  bool sendDirectly = true,
  required String sharedPrefAttachmentName,
  required dynamic sharedPreferences,
  required Function(String) onSendDirectlyNewMessage,
  required Function(String) onSendDirectlyAddMessage,
  required String appLocalCode,
}) async {
  final bool isEnglish = appLocalCode.toLowerCase() == 'de' ? false : true;
  await showCupertinoModalPopup(
    context: context,
    builder: (BuildContext context) => CupertinoActionSheet(
      actions: <Widget>[
        CupertinoActionSheetActionRow(
          onSendDirectlyNewMessage: onSendDirectlyNewMessage,
          onSendDirectlyAddMessage: onSendDirectlyAddMessage,
          sendDirectly: sendDirectly,
          sharedPreferences: sharedPreferences,
          title: isEnglish
              ? TranslationEnglish.TAKE_PICTURE
              : TranslationGerman.TAKE_PICTURE,
          isEnglish: isEnglish,
          iconData: FontAwesomeProIconsHandleAttachment.camera_light,
          isTakePicture: true,
          context: context,
          subject: subject,
          topicId: topicId,
          newMessageTitleControllerText: newMessageTitleControllerText,
          sharedPrefAttachmentName: sharedPrefAttachmentName,
          attachmentFileName: attachmentFileName,
        ),
        // on ios we must allow image_picker because file_picker cannot access all images
        // but on other devices, we disable "image_picker" if only jpeg is allowed to upload
        allowOnlyPictures && !Platform.isIOS
            ? const SizedBox()
            : CupertinoActionSheetActionRow(
                onSendDirectlyNewMessage: onSendDirectlyNewMessage,
                onSendDirectlyAddMessage: onSendDirectlyAddMessage,
                sendDirectly: sendDirectly,
                sharedPreferences: sharedPreferences,
                title: isEnglish
                    ? TranslationEnglish.UPLOAD_PICTURE
                    : TranslationGerman.UPLOAD_PICTURE,
                isEnglish: isEnglish,
                iconData:
                    FontAwesomeProIconsHandleAttachment.image_polaroid_light,
                isUploadPicture: true,
                context: context,
                subject: subject,
                topicId: topicId,
                newMessageTitleControllerText: newMessageTitleControllerText,
                sharedPrefAttachmentName: sharedPrefAttachmentName,
                allowOnlyPictures: allowOnlyPictures,
                attachmentFileName: attachmentFileName,
              ),
        CupertinoActionSheetActionRow(
          isEnglish: isEnglish,
          onSendDirectlyNewMessage: onSendDirectlyNewMessage,
          onSendDirectlyAddMessage: onSendDirectlyAddMessage,
          sendDirectly: sendDirectly,
          sharedPreferences: sharedPreferences,
          title: isEnglish
              ? allowOnlyPictures && Platform.isIOS
                  ? TranslationEnglish.UPLOAD_PICTURE_FROM_FILES
                  : allowOnlyPictures
                      ? TranslationEnglish.UPLOAD_PICTURE
                      : TranslationEnglish.UPLOAD_FILE
              : allowOnlyPictures && Platform.isIOS
                  ? TranslationGerman.UPLOAD_PICTURE_FROM_FILES
                  : allowOnlyPictures
                      ? TranslationGerman.UPLOAD_PICTURE
                      : TranslationGerman.UPLOAD_FILE,
          iconData: allowOnlyPictures
              ? FontAwesomeProIconsHandleAttachment.image_polaroid_light
              : FontAwesomeProIconsHandleAttachment.file_alt_light,
          isUploadFile: true,
          context: context,
          subject: subject,
          topicId: topicId,
          newMessageTitleControllerText: newMessageTitleControllerText,
          sharedPrefAttachmentName: sharedPrefAttachmentName,
          allowOnlyPictures: allowOnlyPictures,
          attachmentFileName: attachmentFileName,
        ),
      ],
      cancelButton: CupertinoActionSheetAction(
        child: Text(
          isEnglish ? TranslationEnglish.CANCEL : TranslationEnglish.CANCEL,
        ),
        isDefaultAction: true,
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    ),
  );
}
