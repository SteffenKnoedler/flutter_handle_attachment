import 'package:equatable/equatable.dart';

import 'inserted_file.dart';

class AttachedFiles extends Equatable {
  final List<InsertedFile?> files;

  const AttachedFiles({
    required this.files,
  });

  @override
  List<Object?> get props => [
        files,
      ];
}
