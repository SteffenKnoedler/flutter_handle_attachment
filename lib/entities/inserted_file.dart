import 'package:equatable/equatable.dart';

class InsertedFile extends Equatable {
  final String fileName;
  final String type;
  final String base64String;

  const InsertedFile({
    required this.fileName,
    required this.type,
    required this.base64String,
  });

  @override
  List<Object?> get props => [
        fileName,
        type,
        base64String,
      ];
}
