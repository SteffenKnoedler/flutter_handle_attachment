import 'package:flutter_handle_attachment/entities/inserted_file.dart';

class InsertedFileModel extends InsertedFile {
  const InsertedFileModel({
    required String base64String,
    required String fileName,
    required String type,
  }) : super(
          base64String: base64String,
          fileName: fileName,
          type: type,
        );

  factory InsertedFileModel.fromJson(Map<String, dynamic> json) {
    return InsertedFileModel(
      base64String: json['base64String'],
      type: json['type'],
      fileName: json['fileName'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "base64String": base64String,
      "type": type,
      "fileName": fileName,
    };
  }
}
