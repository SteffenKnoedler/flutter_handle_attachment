import 'package:flutter_handle_attachment/entities/attached_files.dart';
import 'package:flutter_handle_attachment/entities/inserted_file.dart';

import 'inserted_file_model.dart';

class AttachedFilesModel extends AttachedFiles {
  const AttachedFilesModel({required List<InsertedFile?> files})
      : super(
          files: files,
        );

  factory AttachedFilesModel.fromJson(Map<String, dynamic> json) {
    var list = json['files'] as List;
    List<InsertedFile?> fileList =
        list.map((i) => InsertedFileModel.fromJson(i)).toList();

    return AttachedFilesModel(
      files: fileList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "files": files,
    };
  }
}
