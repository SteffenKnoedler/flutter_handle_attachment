import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_handle_attachment/translation/de.dart';
import 'package:flutter_handle_attachment/translation/en.dart';
import 'package:permission_handler/permission_handler.dart';

Future<bool> checkAndRequestPermission(
    {required BuildContext context,
    required Permission permission,
    required bool isEnglish}) async {
  final String title = isEnglish
      ? TranslationEnglish.NO_ACCESS_TITLE
      : TranslationGerman.NO_ACCESS_TITLE;

  final String body = isEnglish
      ? TranslationEnglish.NO_ACCESS_MESSAGE
      : TranslationGerman.NO_ACCESS_MESSAGE;
  final String action = isEnglish
      ? TranslationEnglish.OPEN_APP_SETTINGS
      : TranslationGerman.OPEN_APP_SETTINGS;

  _showCustomDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) => Platform.isIOS
          ? CustomCupertinoDialog(
              title: title,
              body: body,
              action: action,
            )
          : CustomAndroidDialog(
              title: title,
              body: body,
              action: action,
            ),
    );
  }

  if (await permission.isPermanentlyDenied) {
    _showCustomDialog();
    return false;
  } else {
    if (await permission.request().isGranted) {
      return true;
    }
    return false;
  }
}

Future<void> requestPermission({
  required BuildContext context,
  required Permission permission,
  required bool isEnglish,
}) async {
  final String title = isEnglish
      ? TranslationEnglish.NO_ACCESS_TITLE
      : TranslationGerman.NO_ACCESS_TITLE;

  final String body = isEnglish
      ? TranslationEnglish.NO_ACCESS_MESSAGE
      : TranslationGerman.NO_ACCESS_MESSAGE;
  final String action = isEnglish
      ? TranslationEnglish.OPEN_APP_SETTINGS
      : TranslationGerman.OPEN_APP_SETTINGS;
  _showCustomDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) => Platform.isIOS
          ? CustomCupertinoDialog(
              title: title,
              body: body,
              action: action,
            )
          : CustomAndroidDialog(
              title: title,
              body: body,
              action: action,
            ),
    );
  }

  if (await permission.request().isGranted) {
  } else {
    _showCustomDialog();
  }
}

class CustomAndroidDialog extends StatelessWidget {
  final String title;
  final String body;
  final String action;
  final bool isLocalAuth;
  const CustomAndroidDialog({
    Key? key,
    required this.title,
    required this.body,
    required this.action,
    this.isLocalAuth = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        TextButton(
          child: Text(action),
          onPressed: () {
            Navigator.pop(context);
            isLocalAuth
                ? AppSettings.openSecuritySettings()
                : openAppSettings();
          },
        )
      ],
    );
  }
}

class CustomCupertinoDialog extends StatelessWidget {
  final String title;
  final String body;
  final String action;
  final bool isLocalAuth;
  const CustomCupertinoDialog({
    Key? key,
    required this.title,
    required this.body,
    required this.action,
    this.isLocalAuth = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
          child: Text(action),
          onPressed: () {
            Navigator.pop(context);
            isLocalAuth
                ? AppSettings.openSecuritySettings()
                : openAppSettings();
          },
        )
      ],
    );
  }
}
