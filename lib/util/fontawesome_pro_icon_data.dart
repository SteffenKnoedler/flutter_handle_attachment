import 'package:flutter/widgets.dart';

class FontAwesomeProIconsHandleAttachment {
  FontAwesomeProIconsHandleAttachment._();

  static const _kFontFam = 'FontAwesomeProIconsHandleAttachment';
  static const String? _kFontPkg = 'flutter_handle_attachment';

  static const IconData file_alt_light =
      IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData camera_light =
      IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData image_polaroid_light =
      IconData(0xe81d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
