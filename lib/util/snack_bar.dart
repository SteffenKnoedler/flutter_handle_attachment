import 'package:flutter/material.dart';

showSnackBar(
    {required String content,
    bool red = false,
    required BuildContext context}) {
  return WidgetsBinding.instance!.addPostFrameCallback((_) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: red ? Colors.red : Colors.green,
        content: Text(content),
      ),
    );
  });
}
