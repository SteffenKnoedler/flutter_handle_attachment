import 'dart:convert';
import 'dart:io';

import 'package:image/image.dart';

String convertImage2JPEG({required String path}) {
  final image = decodeImage(File(path).readAsBytesSync())!;
  // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
  final thumbnail = copyResize(image, width: 640);

  String base64Image = base64Encode(encodeJpg(thumbnail));

  return base64Image;
}

String? createBase64StringFromFile(dynamic file) {
  String fileInBase64;
  if (file.path.contains('.pdf')) {
    try {
      // ignore: unnecessary_non_null_assertion
      File convertedFile = File(file.path!);
      List<int> fileInByte = convertedFile.readAsBytesSync();
      fileInBase64 = base64Encode(fileInByte);
      return fileInBase64;
    } catch (e) {
      return null;
    }
  }
  try {
    // ignore: unnecessary_non_null_assertion
    fileInBase64 = convertImage2JPEG(path: file.path!);
    return fileInBase64;
  } catch (e) {
    return null;
  }
}
