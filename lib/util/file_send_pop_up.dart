import 'package:flutter/material.dart';
import 'package:flutter_handle_attachment/translation/de.dart';
import 'package:flutter_handle_attachment/translation/en.dart';

import 'create_message_json_string.dart';
import 'snack_bar.dart';

Future<void> userConfirmationSendChatMessageDialog(
    {required String fileInBase64,
    fileName,
    int? fileSize,
    required BuildContext context,
    required String? subject,
    required final Function(String) onSendDirectlyNewMessage,
    required final Function(String) onSendDirectlyAddMessage,
    required bool isEnlish,
    topicId,
    newMessageTitleControllerText}) async {
  // file size not available when sent via picture
  // hence:
  int fileSizeInt = fileSize ?? 500001;

  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        content: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: ListBody(
            children: <Widget>[
              Text((isEnlish
                      ? TranslationEnglish.SEND
                      : TranslationGerman.SEND) +
                  ' ' +
                  fileName +
                  '?'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              (isEnlish ? TranslationEnglish.CANCEL : TranslationGerman.CANCEL)
                  .toUpperCase(),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text(
              (isEnlish
                      ? TranslationEnglish.CONFIRM_SEND
                      : TranslationGerman.CONFIRM_SEND)
                  .toUpperCase(),
            ),
            onPressed: () async {
              // subject is empty: create message

              if (newMessageTitleControllerText != '' &&
                  newMessageTitleControllerText != null) {
                String messageJsonString = createMessageJsonStringWithFile(
                  fileName: fileName,
                  fileInBase64: fileInBase64,
                  newMessageTitleControllerText: newMessageTitleControllerText,
                );
                onSendDirectlyNewMessage(messageJsonString);

                // if file is bigger than 500kb
                if (fileSizeInt > 500000) {
                  showSnackBar(
                      content: (isEnlish
                          ? TranslationEnglish.UPLOADING
                          : TranslationGerman.UPLOADING),
                      context: context);
                }
              } else {
                String topicJsonString = addMessageJsonStringWithFile(
                  fileName: fileName,
                  fileInBase64: fileInBase64,
                  topicId: topicId,
                );
                onSendDirectlyAddMessage(topicJsonString);

                // if file is bigger than 500kb
                if (fileSizeInt > 500000) {
                  showSnackBar(
                    content: (isEnlish
                        ? TranslationEnglish.UPLOADING
                        : TranslationGerman.UPLOADING),
                    context: context,
                  );
                }
              }
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
