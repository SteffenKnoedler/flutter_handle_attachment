import 'dart:convert';

import 'package:flutter_handle_attachment/entities/attached_files.dart';

// new messages also creates new topic

// No File
String createMessageJsonStringNoFile({
  required String messageTextControllerText,
  required String titleControllerText,
}) {
  var messageJsonUpdate = {};
  messageJsonUpdate['message'] = messageTextControllerText;
  messageJsonUpdate['subject'] = titleControllerText;
  messageJsonUpdate['unread'] = 1;
  messageJsonUpdate['sender_name'] = 'Ich';

  String messageJsonUpdateString = jsonEncode(messageJsonUpdate);
  return messageJsonUpdateString;
}

// File added
String createMessageJsonStringWithFile({
  required String fileName,
  String? newMessageControllerText,
  required String fileInBase64,
  required String newMessageTitleControllerText,
}) {
  var documentJsonUpdate = {};
  documentJsonUpdate['type'] = getFileType(fileName: fileName);
  documentJsonUpdate['data'] = fileInBase64;
  var messageJsonUpdate = {};
  messageJsonUpdate['message'] = newMessageControllerText ?? fileName;
  messageJsonUpdate['subject'] = newMessageTitleControllerText;
  messageJsonUpdate['unread'] = 1;
  messageJsonUpdate['sender_name'] = 'Ich';
  messageJsonUpdate['documents'] = [documentJsonUpdate];

  String messageJsonUpdateString = jsonEncode(messageJsonUpdate);
  return messageJsonUpdateString;
}

// NEWWW
String createMessageJsonStringWithFileNew({
  required AttachedFiles attachedFiles,
  String? newMessageControllerText,
  required String newMessageTitleControllerText,
}) {
  List documentArray = [];
  for (var file in attachedFiles.files) {
    var documentJsonUpdate = {};
    documentJsonUpdate['type'] = file!.type;
    documentJsonUpdate['data'] = file.base64String;
    documentArray.add(documentJsonUpdate);
  }

  var messageJsonUpdate = {};
  messageJsonUpdate['message'] =
      newMessageControllerText ?? attachedFiles.files.first!.fileName;
  messageJsonUpdate['subject'] = newMessageTitleControllerText;
  messageJsonUpdate['unread'] = 1;
  messageJsonUpdate['sender_name'] = 'Ich';
  messageJsonUpdate['documents'] = documentArray;

  String messageJsonUpdateString = jsonEncode(messageJsonUpdate);
  return messageJsonUpdateString;
}

// new messages appends message to existing topic

// no file added
String addMessageJsonStringNoFile({
  required int topicId,
  required String messageTextControllerText,
}) {
  var topicJsonUpdate = {};
  topicJsonUpdate['id'] = topicId;

  var messageJsonUpdate = {};
  messageJsonUpdate['sender_name'] = 'Ich';
  messageJsonUpdate['sender'] = null;
  messageJsonUpdate['message'] = messageTextControllerText;
  messageJsonUpdate['unread'] = 1;
  messageJsonUpdate['id'] = null;
  messageJsonUpdate['documents'] = [];
  topicJsonUpdate['messages'] = [messageJsonUpdate];

  String topicJsonUpdateString = jsonEncode(topicJsonUpdate);
  return topicJsonUpdateString;
}

// file added
String addMessageJsonStringWithFile({
  required String fileName,
  required String fileInBase64,
  required int topicId,
}) {
  var documentJsonUpdate = {};

  documentJsonUpdate['type'] = getFileType(fileName: fileName);
  documentJsonUpdate['data'] = fileInBase64;

  var topicJsonUpdate = {};
  topicJsonUpdate['id'] = topicId;

  var messageJsonUpdate = {};
  messageJsonUpdate['id'] = null;
  messageJsonUpdate['sender_name'] = 'Ich';
  messageJsonUpdate['sender'] = null;
  messageJsonUpdate['message'] = fileName;
  messageJsonUpdate['unread'] = 1;

  messageJsonUpdate['documents'] = [documentJsonUpdate];

  topicJsonUpdate['messages'] = [messageJsonUpdate];
  String topicJsonUpdateString = jsonEncode(topicJsonUpdate);
  return topicJsonUpdateString;
}

// get File ending of filename
String getFileType({String? fileName}) {
  String fileNameString = fileName ?? 'unkown';
  return fileNameString.contains('.pdf')
      ? 'application/pdf'
      : fileNameString.contains('.png')
          ? 'image/png'
          : fileNameString.contains('.jpeg')
              ? 'image/jpeg'
              : fileNameString.contains('.jpg')
                  ? 'image/jpg'
                  : 'unknown';
}

String getFileEnding({String? fileName}) {
  String fileNameString = fileName ?? 'unkown';
  return fileNameString.contains('.pdf')
      ? '.pdf'
      : fileNameString.contains('.png')
          ? '.png'
          : fileNameString.contains('.jpeg')
              ? '.jpeg'
              : fileNameString.contains('.jpg')
                  ? '.jpg'
                  : 'unknown';
}
