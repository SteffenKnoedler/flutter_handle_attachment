import 'dart:convert';

import 'package:flutter_handle_attachment/models/attached_files_model.dart';

// function accesses SharedPref --> AttachedFilesJsonString
// returns AttachedFilesModel
AttachedFilesModel? getInsertedFilesJson(
    {required dynamic sharedPreferences,
    required String sharedPrefAttachmentName}) {
  // make sure SharedPref is not null
  if (sharedPreferences.getString(sharedPrefAttachmentName) != null) {
    AttachedFilesModel attachedFiles = AttachedFilesModel.fromJson(
      json.decode(
        sharedPreferences.getString(sharedPrefAttachmentName)!,
      ),
    );
    return attachedFiles;
  }
  return null;
}
